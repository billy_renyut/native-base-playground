/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import AnatomyScreen from './src/play/AnatomyScreen'
import PlayActionSheetScreen from './src/play/PlayActionSheetScreen'
import PlayBadgeScreen from './src/play/PlayBadgeScreen'
import PlayButtonScreen from './src/play/PlayButtonScreen'
import ThemedButtonScreen from './src/play/buttons/ThemedButtonScreen'
import TransparentButtonScreen from './src/play/buttons/TransparentButtonScreen'
import OutlineButtonScreen from './src/play/buttons/OutlineButtonScreen'
import RoundedButtonScreen from './src/play/buttons/RoundedButtonScreen'
import BlockButtonScreen from './src/play/buttons/BlockButtonScreen'
import FullButtonScreen from './src/play/buttons/FullButtonScreen'
import IconButtonScreen from './src/play/buttons/IconButtonScreen'
import SizeButtonScreen from './src/play/buttons/SizeButtonScreen'
import DisabledButtonScreen from './src/play/buttons/DisabledButtonScreen'

import PlayCardScreen from './src/play/PlayCardScreen'
import CardBasicScreen from './src/play/cards/CardBasicScreen'
import CardHeaderFooterScreen from './src/play/cards/CardHeaderFooterScreen'
import CardListScreen from './src/play/cards/CardListScreen'
import CardImageScreen from './src/play/cards/CardImageScreen'
import CardShowcaseScreen from './src/play/cards/CardShowcaseScreen'

import PlayTestingScreen from './src/play/PlayTestingScreen'


import { StackNavigator } from 'react-navigation'
export default class AwesomeNativeBase extends Component {
  render() {
    return (
      <AnatomyScreen/>
    );
  }
}

const AppNav = StackNavigator({
  Home: { 
    screen: AnatomyScreen,
    headerMode: 'none',
    header: null,
    navigationOptions: {
        header: null
    }
  },
  ActionSheet: { screen: PlayActionSheetScreen },
  Badge: { screen: PlayBadgeScreen },
  Button: { screen: PlayButtonScreen },
  ThemedButton: { screen: ThemedButtonScreen },
  TransparentButton: { screen: TransparentButtonScreen },
  OutlineButton: { screen: OutlineButtonScreen },
  RoundedButton: { screen: RoundedButtonScreen },
  BlockButton: { screen: BlockButtonScreen },
  FullButton: { screen: FullButtonScreen },
  IconButton: { screen: IconButtonScreen },
  DisabledButton: { screen: DisabledButtonScreen },
  Card: { screen: PlayCardScreen },
  CardBasic: { screen: CardBasicScreen },
  CardHeaderFooter: { screen: CardHeaderFooterScreen },
  CardList: { screen: CardListScreen },
  CardImage: { screen: CardImageScreen },
  CardShowcase: { screen: CardShowcaseScreen },
  Ztesting: { screen: PlayTestingScreen },
});

AppRegistry.registerComponent('AwesomeNativeBase', () => AppNav);
