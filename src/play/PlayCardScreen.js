import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  List,
  ListItem,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class PlayCardScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navOptions: [
        {
          route: 'CardBasic',
          text: 'Basic'
        },
        {
          route: 'CardHeaderFooter',
          text: 'Header and Footer'
        },
        {
          route: 'CardList',
          text: 'List'
        },
        {
          route: 'CardImage',
          text: 'Image'
        },
        {
          route: 'CardShowcase',
          text: 'Showcase'
        }
      ]
    }
  }
  static navigationOptions = {
    title: 'Card',
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content padder>
        <List
          dataArray={this.state.navOptions}
          renderRow={(navOpt)=>
          <ListItem onPress={() => navigate(navOpt.route)}>
            <Text>{navOpt.text}</Text>
          </ListItem>
          }
        >
       </List>
     </Content>
      </Container>
    );
  }
}