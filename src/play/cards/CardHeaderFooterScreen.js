import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text,
  Card,
  CardItem
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class CardHeaderFooterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Header and Footer',
  }
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
        <CardItem header>
          <Text>This is header</Text>
        </CardItem>
        <CardItem>
          <Body>
            <Text>
              This is content
            </Text>
          </Body>
        </CardItem>
        <CardItem footer>
          <Text>Footer here</Text>
        </CardItem>
     </Card>
        </Content>
      </Container>
    );
  }
}