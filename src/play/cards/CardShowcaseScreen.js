import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text,
  Card,
  CardItem,
  Thumbnail
} from 'native-base';
import {Image} from 'react-native'
import { StackNavigator } from 'react-navigation'

export default class CardShowcaseScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Showcase',
  }
  render() {
    var imageUri = '../../images/reactnative.png';
    console.log(imageUri);
    return (
      <Container>
        <Content padder>
        <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../../images/reactnative.png')} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>August 30, 2017</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image source={require('../../images/reactnative.png')} style={{height: 200, width: 200, flex: 1}}/>
                <Text>
                  Explain about react native here
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>

        </Content>
      </Container>
    );
  }
}