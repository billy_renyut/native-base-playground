import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text,
  Card,
  CardItem
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class CardListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'List',
  }
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
        <CardItem header>
          <Text>This is List header</Text>
        </CardItem>
        <CardItem>
          <Icon active name="logo-googleplus" />
          <Text>Google Plus</Text>
          <Right>
            <Icon name="arrow-forward" />
          </Right>
        </CardItem>
        <CardItem>
          <Icon active name="logo-facebook" />
          <Text>Facebook</Text>
          <Right>
            <Icon name="arrow-forward" />
          </Right>
        </CardItem>
        <CardItem>
          <Icon active name="logo-twitter" />
          <Text>Twitter</Text>
          <Right>
            <Icon name="arrow-forward" />
          </Right>
        </CardItem>
        <CardItem footer>
          <Text>List Footer here</Text>
        </CardItem>
     </Card>
        </Content>
      </Container>
    );
  }
}