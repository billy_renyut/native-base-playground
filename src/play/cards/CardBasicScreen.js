import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text,
  Card,
  CardItem
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class CardBasicScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Basic',
  }
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
            <CardItem>
              <Body>
                <Text>
                   This is Basic Card
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}