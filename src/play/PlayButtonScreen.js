import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  List,
  ListItem,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class PlayButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navOptions: [
        {
          route: 'ThemedButton',
          text: 'Themed'
        },
        {
          route: 'TransparentButton',
          text: 'Transparent'
        },
        {
          route: 'OutlineButton',
          text: 'Outline'
        },
        {
          route: 'RoundedButton',
          text: 'Rounded'
        },
        {
          route: 'BlockButton',
          text: 'Block'
        },
        {
          route: 'FullButton',
          text: 'Full'
        },
        {
          route: 'IconButton',
          text: 'Icon'
        },
        {
          route: 'SizeButton',
          text: 'Size'
        },
        {
          route: 'DisabledButton',
          text: 'Disabled'
        }
      ]
    }
  }
  static navigationOptions = {
    title: 'Buttons',
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content padder>
        <List
          dataArray={this.state.navOptions}
          renderRow={(navOpt)=>
          <ListItem onPress={() => navigate(navOpt.route)}>
            <Text>{navOpt.text}</Text>
          </ListItem>
          }
        >
       </List>
     </Content>
      </Container>
    );
  }
}