import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class FullButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Full Buttons',
  }
  render() {
    return (
      <Container>
        <Content padder>
          <Button full light>
            <Text>Light</Text>
          </Button>
          <Button full>
            <Text>Primary</Text>
          </Button>
          <Button full success>
            <Text>Success</Text>
          </Button>
          <Button full info>
            <Text>Info</Text>
          </Button>
          <Button full warning>
            <Text>Warning</Text>
          </Button>
          <Button full danger>
            <Text>Danger</Text>
          </Button>
          <Button full dark>
            <Text>Dark</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}