import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class SizeButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Size Buttons',
  }
  render() {
    return (
      <Container>
        <Content padder>
          <Button small primary>
            <Text>Default Small Size</Text>
          </Button>
          <Button success>
            <Text>Success Default Size</Text>
          </Button>
          <Button large dark>
            <Text>Dark Large Size</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}