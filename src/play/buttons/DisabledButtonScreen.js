import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class DisabledButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Size Buttons',
  }
  render() {
    return (
      <Container>
        <Content padder>
        <Button disabled><Text>Default</Text></Button>
          <Button disabled bordered><Text>Bordered</Text></Button>
          <Button disabled rounded><Text>Rounded</Text></Button>
          <Button disabled large><Text>Large</Text></Button>
          <Button disabled iconRight>
            <Text>Icon Button</Text>
            <Icon name="home" />
          </Button>
          <Button disabled block><Text>Block</Text></Button>
          <Button disabled full><Text>Full</Text></Button>
        </Content>
      </Container>
    );
  }
}