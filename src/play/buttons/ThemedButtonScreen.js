import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class ThemedButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Themed Buttons',
  }
  render() {
    return (
      <Container>
        <Content padder>
            <Button light><Text> Light </Text></Button>
            <Button primary><Text> Primary </Text></Button>
            <Button success><Text> Success </Text></Button>
            <Button info><Text> Info </Text></Button>
            <Button warning><Text> Warning </Text></Button>
            <Button danger><Text> Danger </Text></Button>
            <Button dark><Text> Dark </Text></Button>
        </Content>
      </Container>
    );
  }
}