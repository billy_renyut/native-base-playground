import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  Badge,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class IconButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Icon Buttons',
  }
  render() {
    return (
      <Container>
        <Content padder>
        <Button primary>
            <Icon name='ios-home' />
            <Text>Home</Text>
        </Button>

        <Button success iconRight>
            <Text>Next</Text>
            <Icon name='ios-arrow-forward' />
        </Button>

        <Button info>
            <Text>Previous</Text>
            <Icon name='ios-arrow-back' />
        </Button>

        <Button warning>
            <Icon name='ios-star' />
        </Button>

        <Button danger>
            <Icon name='ios-close-circle' />
        </Button>

        <Button style={{backgroundColor: '#384850'}} >
            <Icon name='ios-search' style={{color: '#00c497'}}/>
        </Button>
        </Content>
      </Container>
    );
  }
}