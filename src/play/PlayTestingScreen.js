import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  List,
  ListItem,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class PlayButtonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'Testing',
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content padder>
        <Text>Testing</Text>
     </Content>
      </Container>
    );
  }
}