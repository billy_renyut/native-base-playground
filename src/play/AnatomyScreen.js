import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  List,
  ListItem,
  Text
} from 'native-base';

import { StackNavigator } from 'react-navigation'

export default class AnatomyScreen extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Native Base PG</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
           <List>
            <ListItem onPress={() => navigate('ActionSheet')}>
              <Text>ActionSheet</Text>
            </ListItem>
            <ListItem onPress={() => navigate('Badge')}>
              <Text>Badge</Text>
            </ListItem>
            <ListItem onPress={() => navigate('Button')}>
              <Text>Buttons</Text>
            </ListItem>
            <ListItem onPress={() => navigate('Card')}>
              <Text>Card</Text>
            </ListItem>
            <ListItem onPress={() => navigate('Ztesting')}>
              <Text>Z</Text>
            </ListItem>
          </List>
        </Content>
        <Footer>
          <FooterTab>
            <Button full>
              <Text>Footer</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}