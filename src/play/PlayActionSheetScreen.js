import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Title, 
  Content, 
  Footer, 
  FooterTab, 
  Button, 
  Left, 
  Right, 
  Body, 
  Icon,
  ActionSheet
} from 'native-base';

import{
  Text
} from 'react-native'
import { StackNavigator } from 'react-navigation'

var BUTTONS = [
  'Option 0',
  'Option 1',
  'Option 2',
  'Delete',
  'Cancel',
]
var DESTRUCTIVE_INDEX = 3
var CANCEL_INDEX = 4

export default class PlayActionSheetScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  static navigationOptions = {
    title: 'ActionSheet',
  }
  render() {
    return (
      <Container>
        <Content padder>
          <Button onPress={()=> ActionSheet.show(
          {
            options: BUTTONS,
            cancelButtonIndex: CANCEL_INDEX,
            destructiveButtonIndex: DESTRUCTIVE_INDEX,
            title: 'Testing ActionSheet'
          },
          (buttonIndex) => {
            this.setState({ clicked: BUTTONS[buttonIndex] })
            alert('Option Picked:' + BUTTONS[buttonIndex])
          }
          )}><Text>Show ActionSheet</Text></Button>
        </Content>
      </Container>
    );
  }
}